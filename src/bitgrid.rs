use crate::cell::Cell;

// An optimized 2D grid that stores boolean values
pub struct BitGrid {
    width: usize,
    height: usize,
    data: Vec<u32>,
}
impl BitGrid {
    pub fn new(width: usize, height: usize, initial_value: bool) -> Self {
        let len = (width * height).div_ceil(32);
        let n = if initial_value { 0xFFFFFFFF } else { 0 };
        Self {
            width,
            height,
            data: vec![n; len],
        }
    }
    pub fn set(&mut self, c: Cell, value: bool) {
        assert!(c.x < self.width && c.y < self.height);
        let i = c.y * self.width + c.x;
        let word = self.data[i / 32];
        let new = if value {
            word | (1 << (i % 32))
        } else {
            word & !(1 << (i % 32))
        };
        self.data[i / 32] = new;
    }
    pub fn get(&self, c: Cell) -> bool {
        assert!(c.x < self.width && c.y < self.height);
        let i = c.y * self.width + c.x;
        let word = self.data[i / 32];
        let bit = (word >> (i % 32)) & 1;
        bit == 1
    }
}
