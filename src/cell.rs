use crate::{chemin::Direction, laby::Labyrinthe};

// Represents a maze cell
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Cell {
    pub x: usize,
    pub y: usize,
}
impl From<(usize, usize)> for Cell {
    fn from(value: (usize, usize)) -> Self {
        Cell {
            x: value.0,
            y: value.1,
        }
    }
}
impl From<Cell> for (usize, usize) {
    fn from(val: Cell) -> Self {
        (val.x, val.y)
    }
}
impl Cell {
    pub fn new(x: usize, y: usize) -> Self {
        Cell { x, y }
    }
    pub fn is_neigh(&self, other: Cell) -> bool {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y) == 1
    }
    // Get an adjacent cell
    pub fn adj(&self, dir: Direction) -> Self {
        let (x, y) = match dir {
            Direction::Left => (self.x - 1, self.y),
            Direction::Up => (self.x, self.y - 1),
            Direction::Right => (self.x + 1, self.y),
            Direction::Down => (self.x, self.y + 1),
        };
        Self::new(x, y)
    }
    // Iterate over neighbors
    pub fn neighs<T: Labyrinthe>(&self, l: &T) -> impl Iterator<Item = (Direction, Cell)> {
        let (w, h) = l.dims();
        let x = self.x;
        let y = self.y;
        [
            // Left
            if x == 0 { None } else { Some((x - 1, y)) },
            // Up
            if y == 0 { None } else { Some((x, y - 1)) },
            // Right
            if x == w - 1 { None } else { Some((x + 1, y)) },
            // Down
            if y == h - 1 { None } else { Some((x, y + 1)) },
        ]
        .into_iter()
        .zip(
            [
                Direction::Left,
                Direction::Up,
                Direction::Right,
                Direction::Down,
            ]
            .into_iter(),
        )
        .filter_map(|(o, d)| o.map(|(x, y)| (d, Cell::new(x, y))))
    }
}
