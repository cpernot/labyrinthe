use std::collections::BTreeSet;

use failure::bail;
use image::RgbImage;
use rand::prelude::*;

use crate::{bitgrid::BitGrid, cell::Cell, laby::Labyrinthe, laby_dummy::DummyLaby};

#[derive(Clone, Copy)]
pub enum Direction {
    Left = 0,
    Up = 1,
    Right = 2,
    Down = 3,
}
impl Direction {
    pub fn opp(&self) -> Self {
        match self {
            Direction::Left => Direction::Right,
            Direction::Up => Direction::Down,
            Direction::Right => Direction::Left,
            Direction::Down => Direction::Up,
        }
    }
}
// Represents a path in a maze
pub struct Chemin {
    start: Cell,
    moves: Vec<Direction>,
}
impl Chemin {
    pub fn len(&self) -> usize {
        self.moves.len() + 1
    }
    pub fn append(&mut self, other: &Chemin) {
        self.moves.extend_from_slice(&other.moves);
    }
    pub fn push(&mut self, m: Direction) {
        self.moves.push(m);
    }
    pub fn pop(&mut self) -> Direction {
        self.moves.pop().unwrap()
    }
    // Generate a path from a random walk in a rectangle from (0,0) to given cell
    pub fn random_walk(w: usize, h: usize, end: Cell) -> Self {
        let mut visited = BitGrid::new(w, h, false);
        let l = DummyLaby::create_laby(w, h);
        let c = Cell::new(0, 0);
        visited.set(c, true);
        let mut curr = c;
        let mut path = Self::new(c, Vec::new());
        let mut rng = thread_rng();
        if c == end {
            return path;
        }
        loop {
            if let Some((dir, c)) = curr
                .neighs(&l)
                .filter(|&(_, c)| !visited.get(c))
                .choose(&mut rng)
            {
                curr = c;
                visited.set(c, true);
                path.push(dir);
                if c == end {
                    return path;
                }
            } else {
                // On revient en arrière
                let m = path.pop();
                curr = curr.adj(m.opp())
            }
        }
    }
    // Generate a path from an image.
    // The path must be a black path on a white background
    // The path must begin in (0,0) and end in (width, height)
    pub fn from_image(img: &RgbImage) -> Result<Self, failure::Error> {
        let is_black = |x, y| {
            img.get_pixel(x, y)
                .0
                .into_iter()
                .map(|v| v as u16)
                .sum::<u16>()
                <= 128 * 3
        };
        if !is_black(0, 0) {
            bail!("Le chemin ne commence pas en haut à gauche")
        }
        let w = img.width() as usize;
        let h = img.height() as usize;
        let dummy = DummyLaby::create_laby(w, h);
        if !is_black(img.width() - 1, img.height() - 1) {
            bail!("Le chemin ne finit pas en bas à droite")
        }
        let mut pixels = img
            .enumerate_pixels()
            .filter(|(_, _, p)| p.0.into_iter().map(|v| v as u16).sum::<u16>() <= 128 * 3)
            .map(|(x, y, _)| Cell::new(x as usize, y as usize))
            .collect::<BTreeSet<_>>();
        // On construit le chemin de proche en proche
        let mut c = Cell::new(0, 0);
        let mut cells = [c].to_vec();
        while c != Cell::new(w - 1, h - 1) {
            pixels.remove(&c);
            // On cherche d'abord une case adjacente
            if let Some((_, n)) = c.neighs(&dummy).find(|(_, neigh)| pixels.contains(neigh)) {
                cells.push(n);
                c = n;
                // Sinon, on cherche parmis les cases en diagonales
            } else if let Some((n, dirs)) = [
                if c.x != 0 && c.y != 0 {
                    Some((c.x - 1, c.y - 1))
                } else {
                    None
                },
                if c.x != 0 && c.y != h - 1 {
                    Some((c.x - 1, c.y + 1))
                } else {
                    None
                },
                if c.x != w - 1 && c.y != 0 {
                    Some((c.x + 1, c.y - 1))
                } else {
                    None
                },
                if c.x != w - 1 && c.y != h - 1 {
                    Some((c.x + 1, c.y + 1))
                } else {
                    None
                },
            ]
            .into_iter()
            .zip(
                [
                    [Direction::Left, Direction::Up],
                    [Direction::Left, Direction::Down],
                    [Direction::Right, Direction::Up],
                    [Direction::Right, Direction::Down],
                ]
                .into_iter(),
            )
            .filter_map(|(x, d)| x.map(|cell| (Cell::from(cell), d)))
            .find(|(x, _)| pixels.contains(x))
            {
                if cells.contains(&c.adj(dirs[0])) {
                    cells.push(c.adj(dirs[1]));
                } else {
                    cells.push(c.adj(dirs[0]));
                }
                cells.push(n);
                c = n;
            } else if let Some(cell) = cells.pop() {
                pixels.insert(cell);
            } else {
                bail!("Le chemin donné n'est pas continu.")
            }
        }
        Ok(Self::from_cells(&cells).unwrap())
    }
    pub fn new(start: Cell, moves: Vec<Direction>) -> Self {
        Self { start, moves }
    }
    pub fn begin_end(&self) -> (Cell, Cell) {
        (self.start, self.into_iter().last().unwrap_or(self.start))
    }
    // Generate a path from a list of cells
    pub fn from_cells(cells: &[Cell]) -> Option<Self> {
        let moves = cells
            .windows(2)
            .map(|w| {
                let current = w[0];
                let next = w[1];
                if next.x + 1 == current.x && next.y == current.y {
                    Some(Direction::Left)
                } else if next.x == current.x && next.y + 1 == current.y {
                    Some(Direction::Up)
                } else if next.x == current.x + 1 && next.y == current.y {
                    Some(Direction::Right)
                } else if next.x == current.x && next.y == current.y + 1 {
                    Some(Direction::Down)
                } else {
                    None
                }
            })
            .collect::<Option<Vec<_>>>()?;
        Some(Self {
            start: cells[0],
            moves,
        })
    }
    // Draw the path in red on a given image
    pub fn draw(&self, img: &mut RgbImage) {
        let mut pos = (self.start.x as u32, self.start.y as u32);
        let red = image::Rgb([255, 0, 0]);
        img.put_pixel(2 * pos.0 + 1, 2 * pos.1 + 1, red);
        for d in &self.moves {
            match d {
                Direction::Left => {
                    img.put_pixel(2 * pos.0, 2 * pos.1 + 1, red);
                    img.put_pixel(2 * pos.0 - 1, 2 * pos.1 + 1, red);
                    pos.0 -= 1;
                }
                Direction::Up => {
                    img.put_pixel(2 * pos.0 + 1, 2 * pos.1, red);
                    img.put_pixel(2 * pos.0 + 1, 2 * pos.1 - 1, red);
                    pos.1 -= 1;
                }
                Direction::Right => {
                    img.put_pixel(2 * pos.0 + 2, 2 * pos.1 + 1, red);
                    img.put_pixel(2 * pos.0 + 3, 2 * pos.1 + 1, red);
                    pos.0 += 1;
                }
                Direction::Down => {
                    img.put_pixel(2 * pos.0 + 1, 2 * pos.1 + 2, red);
                    img.put_pixel(2 * pos.0 + 1, 2 * pos.1 + 3, red);
                    pos.1 += 1;
                }
            }
        }
    }
    pub fn start(&self) -> Cell {
        self.start
    }

    pub fn moves(&self) -> &[Direction] {
        self.moves.as_ref()
    }
}
impl<'a> IntoIterator for &'a Chemin {
    type Item = Cell;

    type IntoIter = impl Iterator<Item = Self::Item> + 'a;

    fn into_iter(self) -> Self::IntoIter {
        std::iter::once(self.start).chain(self.moves.iter().scan(self.start, |state, m| {
            match m {
                Direction::Left => {
                    state.x -= 1;
                }
                Direction::Up => {
                    state.y -= 1;
                }
                Direction::Right => {
                    state.x += 1;
                }
                Direction::Down => {
                    state.y += 1;
                }
            }
            Some(*state)
        }))
    }
}
