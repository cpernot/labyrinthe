use std::collections::HashSet;

use failure::{bail, err_msg};
use image::{ImageBuffer, RgbImage};
use rand::prelude::*;

use crate::bitgrid::BitGrid;
use crate::cell::Cell;
use crate::chemin::{Chemin, Direction};
use crate::progressbar::new_bar;

// Trait qui définit une implémentation de labyrinthe et implémente les fonctions de base
pub trait Labyrinthe {
    // Créé et renvoie un labyrinthe avec des murs partout
    fn create_laby(x: usize, y: usize) -> Self;
    // Renvoie les dimensions du labyrinthe
    fn dims(&self) -> (usize, usize);
    // Renvoie vrai si il y a un mur à c1 dans la direction donnée
    fn has_wall(&self, c: Cell, dir: Direction) -> bool;
    // Change l'existence d'un mur d'une cellule
    fn set_wall(&mut self, c: Cell, dir: Direction, s: bool);
}

// Trait qui implémente des algotrithmes sur les labyrinthes
pub trait LabyExt: Sized {
    fn from_solution(solution: &Chemin) -> Result<Self, failure::Error>;
    fn find_solution(&self) -> Result<Chemin, failure::Error>;
    fn find_solution_end(&self, end: Cell) -> Result<Chemin, failure::Error>;
    fn new_random(w: usize, h: usize) -> Self;
    fn to_image(&self) -> RgbImage;
    fn write_ascii(&self, f: &mut impl std::io::Write) -> std::io::Result<()>;
}

impl<T> LabyExt for T
where
    T: Labyrinthe + Sized,
{
    fn find_solution(&self) -> Result<Chemin, failure::Error> {
        let (w, h) = self.dims();
        let end = Cell::new(w - 1, h - 1);
        self.find_solution_end(end)
    }
    // Trouve la solution d'un labyrinthe, et la renvoie si elle existe
    fn find_solution_end(&self, end: Cell) -> Result<Chemin, failure::Error> {
        let (w, h) = self.dims();
        let mut stack = [Cell::new(0, 0)].to_vec();
        let mut visited = BitGrid::new(w, h, false);
        visited.set(Cell::new(0, 0), true);
        let mut visited_count = 1;
        let progress = new_bar((w * h) as u64, "Generating solution");
        while let Some(&c) = stack.last() {
            progress.set_position(visited_count as u64);
            if c == end {
                return Chemin::from_cells(&stack)
                    .ok_or_else(|| err_msg("Le chemin généré est invalide"));
            }
            if let Some((_, adj)) = c
                .neighs(self)
                .find(|&(dir, adj)| !visited.get(adj) && !self.has_wall(c, dir))
            {
                visited.set(adj, true);
                visited_count += 1;
                stack.push(adj);
            } else {
                stack.pop();
            }
        }
        progress.finish_and_clear();
        Err(err_msg("Le labyrinthe n'a pas de solution"))
    }
    // Créé aléatoirement un labyrinthe dont le chemin solution correspond au chemin donné en
    // argument
    fn from_solution(solution: &Chemin) -> Result<Self, failure::Error> {
        let (b, e) = solution.begin_end();
        if b != (0, 0).into() {
            bail!("Le chemin ne commence pas au début du labyrinthe")
        }
        let w = e.x + 1;
        let h = e.y + 1;
        let mut l = T::create_laby(w, h);
        let mut visited = BitGrid::new(w, h, false);
        let mut visited_count = 0;
        // Créé le chemin solution dans le labyrinthe
        {
            let mut c = solution.start();
            for &m in solution.moves() {
                l.set_wall(c, m, false);
                c = c.adj(m);
            }
        }
        // On vérifie que le chemin donné est valide et on marque les cases comme visitées
        for c in solution.into_iter() {
            if visited.get(c) {
                // On est renvenu sur une case déjà visitée: invalide.
                bail!("Le chemin est renvenu sur ses pas en ({},{}).", c.x, c.y);
            } else {
                visited.set(c, true);
                visited_count += 1;
            }
        }
        let mut rng = thread_rng();
        let progress = new_bar((w * h) as u64, "Generating maze");
        for y in 0..h {
            for x in 0..w {
                let cell = Cell::new(x, y);
                if visited.get(cell) {
                    continue;
                }
                let mut walk_set = HashSet::new();
                let mut walk_moves = Vec::new();
                progress.set_position(visited_count as u64);
                let mut curr = cell;
                walk_set.insert(cell);
                // Random walk
                loop {
                    if let Some((dir, c)) = curr
                        .neighs(&l)
                        .filter(|&(_, c)| !walk_set.contains(&c))
                        .choose(&mut rng)
                    {
                        curr = c;
                        walk_moves.push(dir);
                        walk_set.insert(c);
                        if visited.get(c) {
                            // On est renvenu sur une case déjà visitée
                            break;
                        }
                    } else {
                        let m = walk_moves.pop().unwrap();
                        curr = curr.adj(m.opp())
                    }
                }
                // Une fois un chemin trouvé de start_cell à une casé déjà visitée, on enregistre le
                // chemin
                let mut c = cell;
                for &m in &walk_moves {
                    l.set_wall(c, m, false);
                    visited.set(c, true);
                    visited_count += 1;
                    c = c.adj(m);
                }
            }
        }
        progress.finish_and_clear();
        Ok(l)
    }
    // Créé aléatoirement un labyrinthe de w x h
    fn new_random(w: usize, h: usize) -> Self {
        let mut l = Self::create_laby(w, h);
        let mut visited = BitGrid::new(w, h, false);
        visited.set((w - 1, h - 1).into(), true);
        let mut visited_count = 1;
        let mut rng = thread_rng();
        let progress = new_bar((w * h) as u64, "Generating maze");
        for y in 0..h {
            for x in 0..w {
                let cell = Cell::new(x, y);
                if visited.get(cell) {
                    continue;
                }
                let mut walk_set = HashSet::new();
                let mut walk_moves = Vec::new();
                progress.set_position(visited_count as u64);
                let mut curr = cell;
                walk_set.insert(cell);
                // Random walk
                loop {
                    if let Some((dir, c)) = curr
                        .neighs(&l)
                        .filter(|&(_, c)| !walk_set.contains(&c))
                        .choose(&mut rng)
                    {
                        curr = c;
                        walk_moves.push(dir);
                        walk_set.insert(c);
                        if visited.get(c) {
                            // On est renvenu sur une case déjà visitée
                            break;
                        }
                    } else {
                        let m = walk_moves.pop().unwrap();
                        curr = curr.adj(m.opp())
                    }
                }
                // Une fois un chemin trouvé de start_cell à une casé déjà visitée, on enregistre le
                // chemin
                let mut c = cell;
                for &m in &walk_moves {
                    l.set_wall(c, m, false);
                    visited.set(c, true);
                    visited_count += 1;
                    c = c.adj(m);
                }
            }
        }
        progress.finish_and_clear();
        l
    }
    fn to_image(&self) -> RgbImage {
        let (w, h) = self.dims();
        let dimx = (2 * w + 1) as u32;
        let dimy = (2 * h + 1) as u32;
        let wall = image::Rgb([0u8, 0, 0]);
        let empty = image::Rgb([255u8, 255, 255]);
        ImageBuffer::from_fn(dimx, dimy, |x, y| {
            let x = x as usize;
            let y = y as usize;
            match (x % 2, y % 2) {
                (0, 0) => wall,  // Coin de mur
                (1, 1) => empty, // Milieu de case
                (1, 0) => {
                    if (x == 1 && y == 0) || (x == 2 * w - 1 && y == 2 * h) {
                        // Entrée du labyrinthe
                        empty
                    } else if y == 0
                        || y == 2 * h
                        || self.has_wall(Cell::new((x - 1) / 2, y / 2), Direction::Up)
                    {
                        wall
                    } else {
                        empty
                    }
                }
                (0, 1) => {
                    if x == 0
                        || x == 2 * w
                        || self.has_wall(Cell::new(x / 2, (y - 1) / 2), Direction::Left)
                    {
                        wall
                    } else {
                        empty
                    }
                }
                _ => unreachable!(),
            }
        })
    }
    fn write_ascii(&self, f: &mut impl std::io::Write) -> std::io::Result<()> {
        let (w, h) = self.dims();
        for y in 0..(2 * w + 1) {
            for x in 0..(2 * h + 1) {
                let c = match (x % 2, y % 2) {
                    (0, 0) => 'X', // Coin de mur
                    (1, 1) => ' ', // Milieu de case
                    (1, 0) => {
                        if x == 1 && y == 0 {
                            // Entrée du labyrinthe
                            'E'
                        } else if x == 2 * w - 1 && y == 2 * h {
                            // Sortie du labyrinthe
                            'S'
                        } else if y == 0
                            || y == 2 * h
                            || self.has_wall(Cell::new((x - 1) / 2, y / 2), Direction::Up)
                        {
                            'X'
                        } else {
                            ' '
                        }
                    }
                    (0, 1) => {
                        if x == 0
                            || x == 2 * w
                            || self.has_wall(Cell::new(x / 2, (y - 1) / 2), Direction::Left)
                        {
                            'X'
                        } else {
                            ' '
                        }
                    }
                    _ => unreachable!(),
                };
                write!(f, "{}", c)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
