use std::collections::BTreeSet;

use crate::{cell::Cell, chemin::Direction, laby::Labyrinthe};

// Implémentation d'un labyrinthe en utilisant un arbre binaire pour stocker les couples de cases
// adjacentes. Moins rapide que l'implémentation par tableau
pub struct LabyBtr {
    walls: BTreeSet<(Cell, Cell)>,
    width: usize,
    height: usize,
}
impl Labyrinthe for LabyBtr {
    fn create_laby(w: usize, h: usize) -> Self {
        let mut l = Self {
            walls: Default::default(),
            width: w,
            height: h,
        };
        for x in 0..w {
            for y in 0..h {
                let c = Cell::new(x, y);
                l.walls.extend(c.neighs(&l).map(|(_, adj)| (c, adj)));
            }
        }
        l
    }
    fn dims(&self) -> (usize, usize) {
        (self.width, self.height)
    }
    fn has_wall(&self, c: Cell, dir: Direction) -> bool {
        let i = match dir {
            Direction::Left => (c.x - 1, c.y),
            Direction::Up => (c.x, c.y - 1),
            Direction::Right => (c.x + 1, c.y),
            Direction::Down => (c.x, c.y + 1),
        };
        self.walls.contains(&(c, i.into()))
    }

    fn set_wall(&mut self, c: Cell, dir: Direction, s: bool) {
        let i = match dir {
            Direction::Left => (c.x - 1, c.y),
            Direction::Up => (c.x, c.y - 1),
            Direction::Right => (c.x + 1, c.y),
            Direction::Down => (c.x, c.y + 1),
        };
        let c2 = Cell::from(i);
        if s {
            self.walls.insert((c, c2));
            self.walls.insert((c2, c));
        } else {
            self.walls.remove(&(c, c2));
            self.walls.remove(&(c2, c));
        }
    }
}
