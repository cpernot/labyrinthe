use crate::laby::Labyrinthe;

// Impléméntation d'un labyrinthe vide. Utile uniquement pour manipuler des chemins à l'intérieur
pub struct DummyLaby {
    width: usize,
    height: usize,
}
impl Labyrinthe for DummyLaby {
    fn create_laby(w: usize, h: usize) -> Self {
        Self {
            width: w,
            height: h,
        }
    }

    fn dims(&self) -> (usize, usize) {
        (self.width, self.height)
    }

    fn has_wall(&self, _c: crate::cell::Cell, _dir: crate::chemin::Direction) -> bool {
        false
    }

    fn set_wall(&mut self, _c: crate::cell::Cell, _dir: crate::chemin::Direction, _s: bool) {}
}
