use crate::{bitgrid::BitGrid, cell::Cell, chemin::Direction, laby::Labyrinthe};

// Implémentation de labyrinthe par tableau 2D.
// grid[x][2*y] == true: "la case (x,y) a un mur à droite"
// grid[x][2*y+1] == true: "la case (x,y) a un mur en bas"
pub struct LabyGrid {
    grid: BitGrid,
    width: usize,
    height: usize,
}

impl Labyrinthe for LabyGrid {
    fn create_laby(w: usize, h: usize) -> Self {
        Self {
            grid: BitGrid::new(w, 2 * h - 1, true),
            width: w,
            height: h,
        }
    }
    fn dims(&self) -> (usize, usize) {
        (self.width, self.height)
    }
    fn has_wall(&self, c: Cell, dir: Direction) -> bool {
        match dir {
            Direction::Left => {
                if c.x == 0 {
                    false
                } else {
                    self.grid.get((c.x - 1, 2 * c.y).into())
                }
            }
            Direction::Up => {
                if c.y == 0 {
                    false
                } else {
                    self.grid.get((c.x, 2 * c.y - 1).into())
                }
            }
            Direction::Right => {
                if c.x == self.width - 1 {
                    false
                } else {
                    self.grid.get((c.x, 2 * c.y).into())
                }
            }
            Direction::Down => {
                if c.y == self.height - 1 {
                    false
                } else {
                    self.grid.get((c.x, 2 * c.y + 1).into())
                }
            }
        }
    }

    fn set_wall(&mut self, c: Cell, dir: Direction, s: bool) {
        assert!(c.x != 0 || !matches!(dir, Direction::Left));
        assert!(c.y != 0 || !matches!(dir, Direction::Up));
        assert!(c.x != self.width - 1 || !matches!(dir, Direction::Right));
        assert!(c.y != self.height - 1 || !matches!(dir, Direction::Down));
        match dir {
            Direction::Left => {
                self.grid.set((c.x - 1, 2 * c.y).into(), s);
            }
            Direction::Up => {
                self.grid.set((c.x, 2 * c.y - 1).into(), s);
            }
            Direction::Right => {
                self.grid.set((c.x, 2 * c.y).into(), s);
            }
            Direction::Down => {
                self.grid.set((c.x, 2 * c.y + 1).into(), s);
            }
        }
    }
}
