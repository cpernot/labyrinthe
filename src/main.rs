#![feature(type_alias_impl_trait)]
#![feature(int_roundings)]
#![feature(impl_trait_in_assoc_type)]

use chemin::Chemin;

use laby::LabyExt;

use clap::{clap_derive::*, Parser};

mod bitgrid;
mod cell;
mod chemin;
mod laby;
mod laby_btr;
mod laby_dummy;
mod laby_grid;
mod progressbar;

#[derive(Parser)]
struct Params {
    #[command(subcommand)]
    command: Commands,
    #[arg(short, long, help = "Write maze to file")]
    output: String,
    #[arg(
        short,
        long,
        default_value = "false",
        help = "Write maze as ascii file (unsolved only)"
    )]
    ascii: bool,
    #[arg(short, long, help = "Write solved maze to file")]
    solution: Option<String>,
}
#[derive(Subcommand)]
enum Commands {
    Random(RandomArgs),
    Image(ImageArgs),
}
#[derive(Args)]
struct ImageArgs {
    path: String,
    #[arg(long, default_value = "0", help = "Add padding on the left")]
    padding_width: usize,
    #[arg(long, default_value = "0", help = "Add padding on the top")]
    padding_height: usize,
}
#[derive(Args)]
struct RandomArgs {
    width: usize,
    height: usize,
}

fn main() -> Result<(), failure::Error> {
    let args = Params::parse();
    let (l, s) = match args.command {
        // Generate random maze
        Commands::Random(p) => {
            let l = laby_grid::LabyGrid::new_random(p.width, p.height);
            let s = l.find_solution()?;
            (l, s)
        }
        // Generate maze with a given solution pass
        Commands::Image(p) => {
            let img = image::open(p.path)?.to_rgb8();
            // Generate the final path (random + image parts)
            let mut sol = Chemin::random_walk(
                p.padding_width + (img.width() as usize),
                p.padding_height + 1,
                (p.padding_width, p.padding_height).into(),
            );
            sol.append(&Chemin::from_image(&img)?);
            (laby_grid::LabyGrid::from_solution(&sol)?, sol)
        }
    };
    // Save the maze
    let mut img = l.to_image();
    if args.ascii {
        let mut f = std::fs::File::create(args.output)?;
        l.write_ascii(&mut f)?;
    } else {
        img.save(&args.output)?;
    }
    if let Some(path) = args.solution {
        s.draw(&mut img);
        img.save(path)?;
    }
    Ok(())
}
