use indicatif::{ProgressBar, ProgressStyle};

// Create a progress bar in terminal
pub fn new_bar(max: u64, msg: &str) -> ProgressBar {
    let bar = ProgressBar::new(max);
    bar.set_style(
        ProgressStyle::with_template("{msg} {spinner} {percent}%\n{wide_bar} {pos}/{len}").unwrap(),
    );
    bar.set_message(msg.to_owned());
    bar
}
